package com.matteobad.showhidedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Boolean isDisplayed = true;
    Button button;
    TextView text;

    public void change(View view) {
        if (isDisplayed) {
            isDisplayed = false;
            text.setVisibility(View.GONE);
            button.setText("SHOW");

        } else {
            isDisplayed = true;
            text.setVisibility(View.VISIBLE);
            Log.i("VISIBLE", "" + View.VISIBLE);
            button.setText("HIDE");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        text = (TextView) findViewById(R.id.helloWorldTextView);
    }
}
